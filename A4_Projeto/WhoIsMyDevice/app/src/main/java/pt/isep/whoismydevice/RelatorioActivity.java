package pt.isep.whoismydevice;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RelatorioActivity extends AppCompatActivity implements OnClickListener
{

	private String mReport = "Empty";
	private LinearLayout mLayout;
	private String mAppName = "WhoIsMyDevice";

  private String mWify = "WIFI: Desligada";
  private String mMovel = "Dados moveis: Desligados";
  private ConnectivityManager connectivityManager;
  private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
      {
        updateUI();
      }
    }
  };

	private static final String[] PACKAGES = new String[]
  {
  	"pt.isep.whoismydevice"
  };

	private static final String[] SEND_TO_EMAIL = new String[]
  {
    "1010836@isep.ipp.pt"
  };

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_relatorio);
		mLayout = (LinearLayout)findViewById(R.id.layout);
		mReport = "Relatorio de informação do dispositivo:\n";
		try
		{
			PackageInfo info = super.getApplication().getPackageManager().getPackageInfo(getApplication().getPackageName(), 0);

			mAppName  = "  WhoIsMyDevice v"+info.versionName+"("+info.versionCode+")";

      setTextOfLabel(true, "** Aplicação:");
			setTextOfLabel(true, mAppName);
			for(String pkg : PACKAGES)
			{
				setTextOfLabel(false, getPkgVersion(pkg));
			}
			setTextOfLabel(false, "  Localização: "+getResources().getConfiguration().locale.toString());

			setTextOfLabel(true, "** Dispositivo:");
			setTextOfLabel(false, "  Placa: " + android.os.Build.BOARD);
			setTextOfLabel(false, "  Marca: " + android.os.Build.BRAND);
			setTextOfLabel(false, "  Dispositivo: " + android.os.Build.DEVICE);
			setTextOfLabel(false, "  Modelo: "+ android.os.Build.MODEL);
			setTextOfLabel(false, "  Produto: "+ android.os.Build.PRODUCT);
      setTextOfLabel(false, "  Hardware: " + android.os.Build.HARDWARE);
      setTextOfLabel(false, "  Host: " + android.os.Build.HOST);
			setTextOfLabel(false, "  TAGS: "+ android.os.Build.TAGS);
      setTextOfLabel(false, "  Fabricante: " + android.os.Build.MANUFACTURER);
      setTextOfLabel(false, "  Nº Serie: " + android.os.Build.SERIAL);
      setTextOfLabel(false, "  Existe teclado: " + (this.getResources().getConfiguration().keyboard != android.content.res.Configuration.KEYBOARD_NOKEYS));
      setTextOfLabel(false, "  Existe Trackball: " + (this.getResources().getConfiguration().navigation == android.content.res.Configuration.NAVIGATION_TRACKBALL));

      setTextOfLabel(true, "** SO:");
			setTextOfLabel(false, "  Versão de Build  "+android.os.Build.VERSION.RELEASE + ", Inc: '"+android.os.Build.VERSION.INCREMENTAL+"'");
			setTextOfLabel(false, "  Build de exibição: "+android.os.Build.DISPLAY);
			setTextOfLabel(false, "  Finger print: "+android.os.Build.FINGERPRINT);
			setTextOfLabel(false, "  Build ID: "+android.os.Build.ID);
      setTextOfLabel(false, "  Typo de build: " + Build.TYPE);
      setTextOfLabel(false, "  API Level: " + android.os.Build.VERSION.SDK);
      setTextOfLabel(false, "  Data: "+ android.os.Build.TIME);
      setTextOfLabel(false, "  BootLoader : " + android.os.Build.BOOTLOADER);

      DisplayMetrics metrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getMetrics(metrics);

			setTextOfLabel(true, "** Ecrã:");
      setTextOfLabel(false, "  Unidades: "+ Build.USER);
			setTextOfLabel(false, "  Pixels de altura: "+metrics.heightPixels);
			setTextOfLabel(false, "  Pixels de largura: "+metrics.widthPixels);
      setTextOfLabel(false, "  densidade: "+metrics.density);
      setTextOfLabel(false, "  densidade Dpi: "+metrics.densityDpi);
      setTextOfLabel(false, "  densidade Escala: "+metrics.scaledDensity);
      setTextOfLabel(false, "  xdpi: "+metrics.xdpi);
      setTextOfLabel(false, "  ydpi: "+metrics.ydpi);
      setTextOfLabel(false, "  DENSITY_DEFAULT: "+ DisplayMetrics.DENSITY_DEFAULT);
      setTextOfLabel(false, "  DENSITY_LOW: "+ DisplayMetrics.DENSITY_LOW);
      setTextOfLabel(false, "  DENSITY_MEDIUM: "+ DisplayMetrics.DENSITY_MEDIUM);
      setTextOfLabel(false, "  DENSITY_HIGH: "+ DisplayMetrics.DENSITY_HIGH);

      ActivityManager actManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
      ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
      actManager.getMemoryInfo(memInfo);

      setTextOfLabel(false, "**Memoria");
      setTextOfLabel(false,"  Memoria total: " + memInfo.totalMem );
      setTextOfLabel(false,"  Memoria disponivel: " + memInfo.availMem);
      setTextOfLabel(false,"  Com pouca memória: " + memInfo.lowMemory);
      setTextOfLabel(false,"  Limiar minimo: " + memInfo.threshold);
      setTextOfLabel(false,"  Estado do SD Card: ");

      java.util.Properties p = System.getProperties();
      java.util.Enumeration keys = p.keys();
      String key = "";
      while (keys.hasMoreElements())
      {
        key = (String) keys.nextElement();
        setTextOfLabel(false,"    " + key + " = " + (String) p.get(key));
      }

      connectivityManager =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
      updateUI();
      registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

      setTextOfLabel(false, "**Internet");
      setTextOfLabel(false,"  WiFy: " + mWify);
      setTextOfLabel(false,"  Dados moveis: " + mMovel);

      new Handler()
           .postDelayed(
                        new Runnable()
                        {

                          @Override
                          public void run()
                          {
                            setTextOfLabel(true, "** Icone da aplicação:");
                            setTextOfLabel(false, "Largura original: 48");
                            setTextOfLabel(false, "Altura original: 48");
                            ImageView i = (ImageView)findViewById(R.id.app_icon_view);
                            setTextOfLabel(false, "Largura real: "+i.getWidth());
                            setTextOfLabel(false, "Altura real: "+i.getHeight());

                            Button sendEmail = new Button(RelatorioActivity.this);
                            sendEmail.setText("Enviar relatório...");
                            sendEmail.setOnClickListener(RelatorioActivity.this);
                            mLayout.addView(sendEmail);

                          }

                        }
		                    ,250
                       );

	}
	catch (Exception e)
	{
		e.printStackTrace();
		setTextOfLabel(true, "Erro: "+e.toString());
	}
	}

  @Override
  protected void onDestroy()
  {
    unregisterReceiver(broadcastReceiver);
    super.onDestroy();
  }

	private void setTextOfResource(String resName, int resId)
	{
		setTextOfLabel(false, String.format("%s : %s", resName, getText(resId)));
	}

	private String getPkgVersion(String packageName)
	{
		try
		{
			PackageInfo info = getApplication().getPackageManager().getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
			return "  " + packageName+" "+info.versionName+" ("+info.versionCode+")";
		}
		catch (NameNotFoundException e)
		{
			return "Impossivel obter informação sobre '"+packageName+"': "+e.getMessage();
		}
 }

	private void setTextOfLabel(boolean bold, String text)
  {
  	TextView label = new TextView(this);
		label.setText(text);
		label.setTypeface(Typeface.DEFAULT, bold? Typeface.BOLD : Typeface.NORMAL);
		mLayout.addView(label);
		mReport = mReport + "\n" + text;
  }

  private void updateUI(){
    NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
    if (activeNetwork != null) { // connected to the internet
      if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
        mWify = "WIFI: Ligada";
      } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
        mMovel = "Dados moveis: Ligados";
      }
    }
  }

  @Override
	public void onClick(View arg0)
  {

		/* Create the Intent */
		final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

		/* Fill it with Data */
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, SEND_TO_EMAIL);
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mAppName);
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, mReport);
		//emailIntent.putExtra(Intent.EXTRA_STREAM, u);
		/* Send it off to the Activity-Chooser */  
		this.startActivity(Intent.createChooser(emailIntent, "Enviar mail..."));
	}

}