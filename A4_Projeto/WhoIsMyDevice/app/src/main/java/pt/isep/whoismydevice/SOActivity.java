package pt.isep.whoismydevice;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.TextView;

public class SOActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_so);

      TextView lblInfo = (TextView)findViewById(R.id.lblInfo);

      String build = "";

      build += "**SO"  + "\n";

        build += "  Versão de Build  "+ Build.VERSION.RELEASE + ", Inc: '"+ Build.VERSION.INCREMENTAL+"'" + "\n";
        // A build ID string meant for displaying to the user
        build += "  Build de exibição: " + Build.DISPLAY + "\n";
        // A string that uniquely identifies this build.
        build += "  FingerPrint: "+Build.FINGERPRINT + "\n";
        // Either a changelist number, or a label like "M4-rc20".
        build += "  Build ID: " + Build.ID + "\n";
        // The type of build, like "user" or "eng".
        build += "  Typo de build: " + Build.TYPE + "\n";
        build += "  API Level: " + android.os.Build.VERSION.SDK+ "\n";
        build += "  Data: "+ Build.TIME + "\n";
        // The system bootloader version number.
        build += "  BootLoader : " + Build.BOOTLOADER+ "\n";

      lblInfo.setText(build);
  }
}
