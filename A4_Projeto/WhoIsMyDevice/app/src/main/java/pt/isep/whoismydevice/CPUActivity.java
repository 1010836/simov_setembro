package pt.isep.whoismydevice;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class CPUActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_cpu);

      TextView lblInfo = (TextView)findViewById(R.id.lblInfo);

      String build = "";

      build += "**Dispositivo" + "\n";

        // The name of the underlying board, like "goldfish".
        build += "  Placa : " + Build.BOARD + "\n";
        // The consumer-visible brand with which the product/hardware will be associated, if any.
        build += "  Marca " + Build.BRAND + "\n";
        // CPU_ABI deprecated --> use SUPPORTED_ABIS
        // CPU_ABI2 deprecated
        // The name of the industrial design.
        build += "  Dispositivo: " + Build.DEVICE + "\n";
        // The end-user-visible name for the end product.
        build += "  Modelo: " + Build.MODEL + "\n";
        // The name of the overall product.
        build += "  Produto: " + Build.PRODUCT + "\n";
        // The name of the hardware (from the kernel command line or /proc).
        build += "  Hardware: " + Build.HARDWARE + "\n";
        // ?
        build += "  Host: " + Build.HOST + "\n";
        // An ordered list of 32 bit ABIs supported by this device.
        //build += "SUPPORTED_32_BIT_ABIS: " + Build.SUPPORTED_32_BIT_ABIS + "\n";
        // An ordered list of 64 bit ABIs supported by this device.
        //build += "SUPPORTED_64_BIT_ABIS: " + Build.SUPPORTED_64_BIT_ABIS + "\n";
        // An ordered list of ABIs supported by this device.
        //build += "SUPPORTED_ABIS: " + Build.SUPPORTED_ABIS + "\n";
        // Comma-separated tags describing the build, like "unsigned,debug".
        build += "  Tags: " + Build.TAGS + "\n";
        // The manufacturer of the product/hardware.
        build += "  Fabricante: " + Build.MANUFACTURER + "\n";
        // A hardware serial number, if available.
        build += "  Nº Serie: " + Build.SERIAL + "\n";
        build += "  Existe teclado: " + (this.getResources().getConfiguration().keyboard != android.content.res.Configuration.KEYBOARD_NOKEYS) + "\n";
        build += "  Existe Trackball: " + (this.getResources().getConfiguration().navigation == android.content.res.Configuration.NAVIGATION_TRACKBALL) + "\n";

      lblInfo.setText(build);
  }
}
