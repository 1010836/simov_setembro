package pt.isep.whoismydevice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class WifyActivity extends AppCompatActivity {

    private TextView WIFI, MOBILE;
    private ConnectivityManager connectivityManager;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                updateUI();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_wify);

      connectivityManager =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
      updateUI();
      registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();

    }

    private void updateUI(){
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        TextView lblInfo = (TextView)findViewById(R.id.lblInfo);
        String build = "";

        build += "**Internet" + "\n";

        if (activeNetwork != null)
        { // connected to the internet

            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
            {
              build += "  Wify: Ligado" + "\n";
            }
            else
            {
              build += "  Wify: Desligado" + "\n";
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
            {
              build += "  Dados moveis: Ligados" + "\n";
            }
            else
            {
              build += "  Dados moveis: Desligados" + "\n";
            }

        }
        else
        {
          build += "  Wify: Desligado" + "\n";
          build += "  Dados moveis: Desligados" + "\n";
        }

        lblInfo.setText(build);

    }
}
