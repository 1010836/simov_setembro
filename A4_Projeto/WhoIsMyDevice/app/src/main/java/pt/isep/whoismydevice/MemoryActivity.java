package pt.isep.whoismydevice;

import android.app.ActivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MemoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_memory);

      TextView lblMemory = (TextView)findViewById(R.id.lblMemory);

      ActivityManager actManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
      ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
      actManager.getMemoryInfo(memInfo);

      String build = "";

      build += "**Memoria" + "\n";
      build += "  Memoria total: " + memInfo.totalMem + "\n";
      build += "  Memoria disponivel: " + memInfo.availMem + "\n";
      build += "  Com pouca memória: " + memInfo.lowMemory + "\n";
      build += "  Limiar minimo: " + memInfo.threshold + "\n";
      build += "  Estado do SD Card: " + "\n";

      java.util.Properties p = System.getProperties();
      java.util.Enumeration keys = p.keys();
      String key = "";
      while (keys.hasMoreElements())
      {
        key = (String) keys.nextElement();
        build += "    " + key + " = " + (String) p.get(key) + "\n";
      }

      lblMemory.setText(build);

    }
}
