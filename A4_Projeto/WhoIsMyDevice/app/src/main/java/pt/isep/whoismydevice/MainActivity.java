package pt.isep.whoismydevice;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCPU = (Button)findViewById(R.id.btnCPU);
        Button btnSO = (Button)findViewById(R.id.btnSO);
        Button btnEcra = (Button)findViewById(R.id.btnEcra);
        Button btnMemory = (Button)findViewById(R.id.btnMemory);
        Button btnSensors = (Button)findViewById(R.id.btnSensors);
        Button btnWify = (Button)findViewById(R.id.btnWify);
        Button btnReport = (Button)findViewById(R.id.btnReport);

        btnCPU.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
          Intent i = new Intent(MainActivity.this, CPUActivity.class);
          startActivity(i);
            }
        });

        btnSO.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
          Intent i = new Intent(MainActivity.this, SOActivity.class);
          startActivity(i);
            }
        });

        btnEcra.setOnClickListener(new View.OnClickListener(){
          public void onClick(View arg0) {
            Intent i = new Intent(MainActivity.this, EcraActivity.class);
            startActivity(i);
          }
        });

        btnMemory.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
                Intent i = new Intent(MainActivity.this, MemoryActivity.class);
                startActivity(i);
            }
        });

        btnSensors.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
                Intent i=  new Intent(MainActivity.this, SensorsActivity.class);
                startActivity(i);
            }
        });

        btnWify.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
                Intent i=  new Intent(MainActivity.this, WifyActivity.class);
                startActivity(i);
            }
        });

        btnReport.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
                Intent i=  new Intent(MainActivity.this, RelatorioActivity.class);
                startActivity(i);
            }
        });

    }
}
