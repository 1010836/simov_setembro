package pt.isep.whoismydevice;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.TextView;

public class EcraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_ecra);

      TextView lblInfo = (TextView)findViewById(R.id.lblInfo);

      String build = "";

      DisplayMetrics metrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getMetrics(metrics);

      build += "**Ecrã" + "\n";

        build += "  Unidades: "+ Build.USER + "\n";
        build += "  Pixels de altura: "+ metrics.heightPixels + "\n";
        build += "  Pixels de largura: "+ metrics.widthPixels + "\n";
        build += "  densidade: "+metrics.density + "\n";
        build += "  densidade Dpi: "+metrics.densityDpi + "\n";
        build += "  densidade Escala: "+metrics.scaledDensity + "\n";
        build += "  xdpi: "+metrics.xdpi + "\n";
        build += "  ydpi: "+metrics.ydpi + "\n";
        build += "  DENSITY_DEFAULT: "+ DisplayMetrics.DENSITY_DEFAULT + "\n";
        build += "  DENSITY_LOW: "+ DisplayMetrics.DENSITY_LOW + "\n";
        build += "  DENSITY_MEDIUM: "+ DisplayMetrics.DENSITY_MEDIUM + "\n";
        build += "  DENSITY_HIGH: "+ DisplayMetrics.DENSITY_HIGH + "\n";

      lblInfo.setText(build);
  }
}
