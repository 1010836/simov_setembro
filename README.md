General description

This application provides hardware information about the device where it is installed. It gives you de possibility to see in real time the parameters of some interesting features of your system, like processor frequency, available sensors, etc. It also allows Bluetooth communication between two Android devices with the app installed, sharing and comparing both devices.
Main information

    Sensors info (GPS, Wifi, etc ...).
    Info Processor (Number of Colors, Max Frequency, Real Time Frequency, etc ...).
    Applications info (App size, version, update etc ...)
    RAM information (total RAM, available RAM, etc ...).
    Storage of Spatial Information (Internal, External, SD Card, etc ...)
    Temperature information (system temperature and battery temperature).
    Battery information (battery level, status, type, etc ...)

Who do I talk to?

    Evaristo Figueiredo - 1010836@isep.ipp.pt

Recent activity
